import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { OfertasComponent } from './components/dashboard/ofertas/ofertas.component';
import { ListaofertasComponent } from './components/dashboard/listaofertas/listaofertas/listaofertas.component';
import { GestioncomidaComponent } from './components/dashboard/comida/gestioncomida/gestioncomida.component';
import { GestionbebidaComponent } from './components/dashboard/bebida/gestionbebida/gestionbebida.component';
import { PedidosComponent } from './components/dashboard/pedidos/pedidos.component';
import { SlideMenuComponent } from './slide-menu/slide-menu.component';
import { ListacomidaComponent } from './components/dashboard/comida/Mesas/listacomida.component';
import { GestionarCargosComponent } from './components/dashboard/gestionar-cargos/gestionar-cargos.component';
import { NotFoundComponent } from './components/dashboard/not-found/not-found.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'dashboard', component: SlideMenuComponent,
children: [
  {path: '', component: DashboardComponent },
  {path: 'pedidos', component: PedidosComponent },
  {path: 'ofertas dia', component: OfertasComponent },
  {path: 'listar ofertas', component: ListaofertasComponent },
  {path: 'gestionar_comida', component: GestioncomidaComponent },
  {path: 'gestionar mesas', component: ListacomidaComponent },
  {path: 'gestionar bebidas', component: GestionbebidaComponent },
  {path: 'gestionar cargos', component:  GestionarCargosComponent},
]},
{ path: '**', component: NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
