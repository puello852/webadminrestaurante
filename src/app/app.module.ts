import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpModule } from '@angular/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { OfertasComponent } from './components/dashboard/ofertas/ofertas.component';
import { FuncionesService } from './servicios/funciones.service';
import { ListaofertasComponent } from './components/dashboard/listaofertas/listaofertas/listaofertas.component';
import { GestioncomidaComponent } from './components/dashboard/comida/gestioncomida/gestioncomida.component';
import { GestionbebidaComponent } from './components/dashboard/bebida/gestionbebida/gestionbebida.component';
import { ListabebidaComponent } from './components/dashboard/bebida/listabebida/listabebida.component';
import { ListacomidaComponent } from './components/dashboard/comida/Mesas/listacomida.component';
import { PedidosComponent } from './components/dashboard/pedidos/pedidos.component';
import { MaterialModule } from '../app/material';
import { ApiService } from './servicios/apis/api.service';
import { NgxSpinnerModule } from 'ngx-spinner';
import { ToastrModule } from 'ngx-toastr';
import { SlideMenuComponent } from './slide-menu/slide-menu.component';
import { LayoutModule } from '@angular/cdk/layout';
import { ChartsModule } from 'ng2-charts';
import { MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule } from '@angular/material';
import { GestionarCargosComponent } from './components/dashboard/gestionar-cargos/gestionar-cargos.component';
import { NotFoundComponent } from './components/dashboard/not-found/not-found.component';
import { SidenavService } from './sidenav.service';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    DashboardComponent,
    OfertasComponent,
    ListaofertasComponent,
    GestioncomidaComponent,
    GestionbebidaComponent,
    ListabebidaComponent,
    ListacomidaComponent,
    PedidosComponent,
    SlideMenuComponent,
    GestionarCargosComponent,
    NotFoundComponent
  ],
  imports: [
    ChartsModule,
    BrowserModule,
    MaterialModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      positionClass: 'toast-bottom-right',
    }),
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    HttpClientModule,
    RouterModule,
    NgxSpinnerModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule
  ],
  exports: [
    MaterialModule
  ],
  providers: [NavbarComponent, FuncionesService, ApiService,SidenavService],
  bootstrap: [AppComponent]
})
export class AppModule { }
