import { Component, ViewChild } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, withLatestFrom, filter } from 'rxjs/operators';
import {DomSanitizer} from '@angular/platform-browser';
import {MatIconRegistry} from '@angular/material';
import { Router, NavigationEnd } from '@angular/router';
import { MatSidenav } from '@angular/material';

@Component({
  selector: 'app-slide-menu',
  templateUrl: './slide-menu.component.html',
  styleUrls: ['./slide-menu.component.css'],
})
export class SlideMenuComponent {
  @ViewChild('drawer') drawer: MatSidenav;

  
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  constructor(private breakpointObserver: BreakpointObserver, 
    iconRegistry: MatIconRegistry, sanitizer: DomSanitizer, private rutas: Router) {
    iconRegistry.addSvgIcon(
      'thumbs-up',
      sanitizer.bypassSecurityTrustResourceUrl('assets/icon/food.svg'));
      iconRegistry.addSvgIcon(
        'mesa',
        sanitizer.bypassSecurityTrustResourceUrl('assets/icon/table.svg'));
        iconRegistry.addSvgIcon(
          'cargo',
          sanitizer.bypassSecurityTrustResourceUrl('assets/icon/man.svg'));
          iconRegistry.addSvgIcon(
            'bedida',
            sanitizer.bypassSecurityTrustResourceUrl('assets/icon/bebida.svg'));

  }

  closemenu(){
    this.drawer.close()
  }

  ruta(){
      //this.drawer.close()
        this.rutas.navigate(['dashboard/gestionar_comida'])
  }




}
